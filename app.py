from pytube import YouTube, exceptions
from flask import Flask, send_file, jsonify, redirect, url_for, render_template, request, after_this_request, flash
import os
import os.path


app = Flask(__name__)
@app.route("/")
def home():
    return render_template("index.html")

@app.route('/download', methods=["POST", "GET"])
def test():
    @after_this_request
    def remove_file(response):
        try:
            if path.movie_path():
                os.remove(movie_path)
            else:
                app.logger.error("File Not created", error)
           # send_file.close()
        except Exception as error:
            app.logger.error("Error removing or closing downloaded file handle", error)
        return response
    if request.method == "POST":
        movie_id = request.form["movie_info"]
        movie_format = request.form["movie_extension"]
        try:
            movie = YouTube(movie_id)
            movie_path = movie.title + '.' + (movie_format)
            if movie_format == "mp3":
                stream = movie.streams.get_by_itag(251)
            else:
                stream = movie.streams.get_by_itag(22)
                movie.streams.filter(file_extension='mp4')

            return send_file( stream.download(filename=(movie_path)),  as_attachment=True )
        except Exception as error:
            return redirect('/error')
    else:
        return render_template("download.html")

@app.errorhandler(404)
def page_not_found(name):
    return render_template("404.html"), 404

if __name__ == '__main__':
    app.run()
